import React, { Component } from 'react';


class Forcast extends Component {
    render() {
        return (
            <div className="Forcast">

                <div>{this.props.day}</div>
                <div>{this.props.date}</div>
                <div><img style={{ height: 50 }} src={this.props.picture} alt={this.props.weather} /></div>
                <div>{this.props.weather}</div>
                <div>High: {this.props.high}&#176;C</div>
                <div>Low: {this.props.low}&#176;C</div>
            </div>
        );
    }
}

export default Forcast;
