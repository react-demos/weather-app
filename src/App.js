import React, { Component } from 'react';
import './App.css';
import CityWeather from './CityWeather';

const cities = [
  "Vancouver, BC",
  "Calgary, AB",
  "Toronto, ON",
  "Victoria, BC",
  "Montreal, QC",
  "Edmonton, AB",
  "Ottawa, ON",
  "Winnipeg, MB",
  "Fredericton, NB",
  "St John, NL",
  "Halifax, NS"
]

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: "Vancouver, BC"
    }
    this.changeCity = this.changeCity.bind(this);
  }

  changeCity(e) {
    console.log(e);
    this.setState({ city: e.target.value });
  }

  render() {
    return (
      <div className="App">
        <CityWeather
          cityName={this.state.city}
          key={this.state.city}
        />

        <select name="citySelector" value={this.state.city} onChange={this.changeCity}>
          {cities.map((city) =>
            <option key={city} value={city}>{city}</option>
          )}
        </select>
      </div>
    );
  }
}

export default App;
