import React, { Component } from 'react';


class CurrentWeatherCondition extends Component {
    render() {
        return (
            <div className="CurrentWeatherCondition">
                <div>{this.props.weather}</div>
                <div><img style={{ height: 50 }} src={this.props.picture} alt={this.props.weather} /></div>
                <div>{this.props.temperature}&#176;C</div>
            </div>
        );
    }
}

export default CurrentWeatherCondition;