import React, { Component } from 'react';
import axios from 'axios';
import Forcast from './Forcast';
import CurrentWeatherCondition from './WeatherCondition';

const tempimages = {
    "Windy Rain" : "http://l.yimg.com/a/i/us/we/52/1.gif",
    "Rain And Snow" : "http://l.yimg.com/a/i/us/we/52/5.gif",
    "Partly Cloudy": "http://l.yimg.com/a/i/us/we/52/30.gif",
    "Scattered Showers": "http://l.yimg.com/a/i/us/we/52/9.gif",
    "Sunny": "http://l.yimg.com/a/i/us/we/52/32.gif",
    "Mostly Sunny": "http://l.yimg.com/a/i/us/we/52/32.gif",
    "Thunderstorms": "http://l.yimg.com/a/i/us/we/52/17.gif", //3 or 17
    "Scattered Thunderstorms": "http://l.yimg.com/a/i/us/we/52/3.gif", //37 or 38?
    "Cloudy": "http://l.yimg.com/a/i/us/we/52/26.gif",
    "Mostly Cloudy": "http://l.yimg.com/a/i/us/we/52/28.gif",
    "Rain": "http://l.yimg.com/a/i/us/we/52/6.gif", //6, 11, 18
    "Snow": "http://l.yimg.com/a/i/us/we/52/14.gif",
    "Showers": "http://l.yimg.com/a/i/us/we/52/12.gif",  //12
    "Icy Rain" : "http://l.yimg.com/a/i/us/we/52/10.gif",
    "Breezy" : "http://l.yimg.com/a/i/us/we/52/23.gif",
    "Light Flurries" : "http://l.yimg.com/a/i/us/we/52/13.gif",
    "Heavy SnowFall" : "http://l.yimg.com/a/i/us/we/52/16.gif",
    "Snow Storm" : "http://l.yimg.com/a/i/us/we/52/15.gif",
    "Dust" : "http://l.yimg.com/a/i/us/we/52/19.gif",
    "Fog" : "http://l.yimg.com/a/i/us/we/52/20.gif",
    "Haze" : "http://l.yimg.com/a/i/us/we/52/21.gif",
    "Smoke" : "http://l.yimg.com/a/i/us/we/52/22.gif",
    "Frigid" : "http://l.yimg.com/a/i/us/we/52/25.gif",
}


class CityWeather extends Component {
    constructor(props) {
        super(props);
        this.state = {
            temp: "",
            weather: "",
            forecast: []

        }
        this.farenheitToCelcius = this.farenheitToCelcius.bind(this);
    }

    farenheitToCelcius(temp) {
        let celcius = Math.round((temp - 32) * 5 / 9 * 10) / 10;
        return celcius;
    }


    componentDidMount() {
        axios.get(`https://query.yahooapis.com/v1/public/yql?format=json&q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="${this.props.cityName}")`).then(
            (response) => {
                //console.log(response.data.query.results.channel);
                let temp = this.farenheitToCelcius(response.data.query.results.channel.item.condition.temp);
                let weather = response.data.query.results.channel.item.condition.text;
                let forecast = response.data.query.results.channel.item.forecast
                this.setState({
                    temp,
                    weather,
                    forecast: forecast
                })
            }
        )
    }

    render() {
        return (
            <div className="CityWeather">
                <h1>{this.props.cityName}</h1>
                <CurrentWeatherCondition
                    weather={this.state.weather}
                    picture={tempimages[this.state.weather]}
                    temperature={this.state.temp}

                />
                <h2>Forcast</h2>
                <table border="1">
                    <tbody>
                        <tr>
                            {this.state.forecast.map((day) =>
                                <td key={day.date} valign="top">
                                    <Forcast
                                        picture={tempimages[day.text]}
                                        day={day.day}
                                        date={day.date}
                                        weather={day.text}
                                        high={this.farenheitToCelcius(day.high)}
                                        low={this.farenheitToCelcius(day.low)}
                                    />
                                </td>
                            )}
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default CityWeather;